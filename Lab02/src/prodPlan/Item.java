package prodPlan;

public class Item {
	public Item(Parte parte, int quantidade){
		this.parte = parte;
		this.quantidade = quantidade;
	}
	Parte parte;
	int quantidade;
	
	public float calculaValor() {
		return quantidade*parte.calculaValor();
	}
}
