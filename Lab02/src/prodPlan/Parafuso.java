package prodPlan;

public class Parafuso extends Parte {

	public Parafuso(int codigo, String nome, String descricao, float valor, float comprimento, float diametro) {
		super(codigo, nome, descricao, valor);
		this.comprimento = comprimento;
		this.diametro = diametro;
	}
	
	float comprimento;
	float diametro;
}
